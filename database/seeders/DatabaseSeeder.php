<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Label;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GeoSeeder::class);

        $countries = Country::all();

        $users = \App\Models\User::factory()->count(10)->create();

        $projects = \App\Models\Project::factory()->count(10)->create();

        $labels = Label::factory()->count(10)->create();

        $users->each(function (User $user) use ($countries) {
            $user->country()->associate($countries->random());
            $user->save();
        });

        $projects->each(function (Project $project) use ($users) {
            $project->author()->associate($users->random());
            $project->save();
        });

        $projects->each(function (Project $project) use ($users) {
            $project->users()->attach($users->random(rand(2, 3)));
        });

        $labels->each(function (Label $label) use ($projects) {
            $label->projects()->attach($projects->random(rand(3, 5))->pluck('id'));
        });
    }
}
