<?php

namespace Database\Seeders;

use App\Models\Continent;
use App\Models\Country;
use App\Models\Label;
use App\Models\Project;
use Illuminate\Database\Seeder;

class GeoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $path = database_path('data' . DIRECTORY_SEPARATOR . 'continents.json');

        $content = file_get_contents($path);

        $items = json_decode($content, true);

        $data = [];
        foreach (array_unique($items) as $item) {
            $data[] = [
                'code' => $item,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        Continent::insert($data);

        $continents = Continent::all()->pluck('id', 'code');

        $path = database_path('data' . DIRECTORY_SEPARATOR . 'countries.json');

        $content = file_get_contents($path);

        $countryItems = json_decode($content, true);

        $data = [];
        foreach ($items as $countryCode => $continentCode) {
            $data[] = [
                'code' => $countryCode,
                'name' => $countryItems[$countryCode],
                'continent_id' => $continents[$continentCode],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        Country::insert($data);
    }
}
