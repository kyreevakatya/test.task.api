# Test Task API

To deploy a test task, use the commands:

docker run --rm --interactive --tty --volume $(pwd):/app composer install

docker-compose up -d

php artisan migrate --seed
