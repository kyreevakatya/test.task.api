<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use http\Client\Request;

class UserController
{
    public function index()
    {
        $users = User::all();

        return response()->json($users);


    }
    public function create()
    {

    }
    public function store(Request $request)
    {

    }
    public function show(User $user)
    {

    }
    public function edit()
    {

    }
    public function update(User $user, Request $request)
    {

    }
    public function destroy(User $user)
    {

    }

}
